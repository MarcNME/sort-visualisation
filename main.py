import math
import random
import time
import tkinter as tk
from tkinter import ttk, Canvas

DATA_SIZE = 50
DELAY = 0.0


def swap(arr: [], a, b):
    tmp = arr[a]
    arr[a] = arr[b]
    arr[b] = tmp


def randData():
    data = []
    r = random.Random()
    r.seed(time.time_ns())

    for i in range(DATA_SIZE):
        data.append(r.randint(0, 500))

    return data


def sortedData():
    data = []
    for i in range(DATA_SIZE):
        data.append(i)

    return data


def reversedData():
    data = []
    for i in range(DATA_SIZE, 0, -1):
        data.append(i)

    return data


class MainFrame(ttk.Frame):
    def __init__(self, container):
        super().__init__(container)
        self.selected = tk.StringVar(value="rand")

        self.rowconfigure(1, weight=2)
        self

        self.__create_widgets()

    def __create_widgets(self):
        ttk.Radiobutton(self, text="Random Values", value="rand", variable=self.selected).grid(row=0, column=0)
        ttk.Radiobutton(self, text="Sorted Values", value="sorted", variable=self.selected).grid(row=0, column=1)
        ttk.Radiobutton(self, text="Reversed Values", value="reversed", variable=self.selected).grid(row=0, column=2)

        ttk.Button(self, text="Bubble-Sort", command=self.bubbleSort).grid(row=1, column=0)
        ttk.Button(self, text="Quick-Sort", command=self.quickSortStart).grid(row=1, column=1)
        ttk.Button(self, text="Insertion-Sort", command=self.insertionSort).grid(row=1, column=2)
        ttk.Button(self, text="Slow-Sort", command=self.slowSortStart).grid(row=2, column=0)

        self.lblStatus = ttk.Label(self, text="Status:")
        self.lblStatus.grid(row=3, columnspan=3)

        self.canvas = Canvas(self, height=500, width=500)
        self.canvas.grid(row=4, columnspan=3)

        for widget in self.winfo_children():
            widget.grid(padx=5, pady=5)

    def getData(self):
        match self.selected.get():
            case "rand":
                return randData()
            case "sorted":
                return sortedData()
            case "reversed":
                return reversedData()

    def drawData(self, data):
        y = 0
        self.canvas.delete("all")

        for i in range(len(data)):
            self.canvas.create_rectangle(y, 0, y + 5, data[i], fill="blue")
            self.update()
            y += 10

    def bubbleSort(self):
        self.lblStatus.config(text="Bubble-Sort Running")

        data = self.getData()

        self.drawData(data)

        for n in range(len(data), 0, -1):
            i = 0
            while i < n - 1:
                if data[i] > data[i + 1]:
                    swap(data, i, i + 1)
                    self.drawData(data)
                i += 1
                time.sleep(DELAY)

        self.lblStatus.config(text="Bubble-Sort Finished")

    def quickSortStart(self):
        self.lblStatus.config(text="Quick-Sort Running")

        data = self.getData()

        self.drawData(data)

        self.quickSort(data, 0, len(data) - 1)
        self.lblStatus.config(text="Quick-Sort finished")

    def quickSort(self, array, low, high):
        if low < high:
            # Find pivot element such that
            # element smaller than pivot are on the left
            # element greater than pivot are on the right
            pi = self.partition(array, low, high)

            # Recursive call on the left of pivot
            self.quickSort(array, low, pi - 1)

            # Recursive call on the right of pivot
            self.quickSort(array, pi + 1, high)

        time.sleep(DELAY)

    def partition(self, array, low, high):
        # choose the rightmost element as pivot
        pivot = array[high]

        # pointer for greater element
        i = low - 1

        # traverse through all elements
        # compare each element with pivot
        for j in range(low, high):
            if array[j] <= pivot:
                # If element smaller than pivot is found
                # swap it with the greater element pointed by i
                i = i + 1

                # Swapping element at i with element at j
                (array[i], array[j]) = (array[j], array[i])
                self.drawData(array)
            time.sleep(DELAY)

        # Swap the pivot element with the greater element specified by i
        (array[i + 1], array[high]) = (array[high], array[i + 1])

        # Return the position from where partition is done
        return i + 1

    def insertionSort(self):
        self.lblStatus.config(text="Insertion-Sort running")
        data = self.getData()
        self.drawData(data)

        for i in range(0, DATA_SIZE - 1):
            value_to_sort = data[i]
            j = i
            while j > 0 and data[j - 1] > value_to_sort:
                data[j] = data[j - 1]
                self.drawData(data)
                j -= 1

            data[j] = value_to_sort
            self.drawData(data)

        self.drawData(data)

    def slowSortStart(self):
        self.lblStatus.config(text="Slow-Sort started")
        data = self.getData()

        self.slowSort(data, 0, DATA_SIZE - 1)
        self.lblStatus.config(text="Slow-Sort finished")

    def slowSort(self, array, i, j):
        if i >= j:
            return

        m = math.floor((i + j) / 2)
        self.slowSort(array, i, m)
        self.slowSort(array, m + 1, j)

        if array[j] < array[m]:
            swap(array, j, m)
            self.drawData(array)
        self.slowSort(array, i, j - 1)


class SortVisualisation(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Sort Visualisation")
        self.__create_widgets()

    def __create_widgets(self):
        MainFrame(self).pack()


if __name__ == '__main__':
    app = SortVisualisation()
    app.mainloop()
